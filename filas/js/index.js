const simulete = $('#btn-simulate');
const trash = $('#btn-trash');
const inpTime = $("#tempo");
const maskInput = $(".mask");

trash.click(() => {
    $('input[type=text]').val('');
    $('.resultado-3').empty();
});

simulete.click((event) => {
    getCalc(event);
});

maskInput.change(() => {
    maskInput.mask('###0.00', {reverse: true});
})

const getCalc = (event) => {
    event.preventDefault();
    let letras = "abc";
    let time = parseFloat(inpTime.val());
    for (let i = 0; i < letras.length; i++){
        let letra = letras[i];
        let temp = parseFloat($(`.entrada .chegada-${letra}`).val());
        temp = time / temp;
        $(`.resultado-1 .chegada-${letra}`).val(temp.toFixed(2));
    }
    for (let i = 0; i < letras.length; i++){
        let letra = letras[i];
        let temp = parseFloat($(".entrada .servico-" + letra).val());
        temp = time / temp;
        $(`.resultado-1 .servico-${letra}`).val(temp.toFixed(2));
    }
    
    //calculo da terceita tabela
    for (let i = 0; i < letras.length; i++){
        let letra = letras[i];
        let a = parseFloat($(`.resultado-1 .chegada-${letra}`).val());
        let u = parseFloat($(`.resultado-1 .servico-${letra}`).val());
        let temp = a/(u-a);
        $(`.resultado-2 .r1-${letra}`).val(temp.toFixed(2));
    }
    for (var i = 0; i < letras.length; i++){
        let letra = letras[i];
        let a = parseFloat($(`.resultado-1 .chegada-${letra}`).val());
        let u = parseFloat($(`.resultado-1 .servico-${letra}`).val());
        let temp = 1.0/(u-a);
        $(`.resultado-2 .r2-${letra}`).val(temp.toFixed(2));
    }
    for (var i = 0; i < letras.length; i++){
        let letra = letras[i];
        let a = parseFloat($(`.resultado-1 .chegada-${letra}`).val());
        let u = parseFloat($(`.resultado-1 .servico-${letra}`).val());
        let temp = a/u;
        $(`.resultado-2 .r3-${letra}`).val(temp.toFixed(2));
    }
    
    let max = $("input#max-clientes").val();
    let components = "<tr><th>N</th><th>P(x)</th><th>Cenário (A)</th><th>Cenário (B)</th><th>Cenário (C)</th></tr>";
    $(".resultado-3").html(components);
    for (let i = 0; i < max; i++){
        let html = "<tr><td>";
        html += i;
        html += "</td><td>";
        html += "p(" + i + ")";
        for (let y = 0; y < letras.length; y++){
            let letra = letras[y];
            let a = parseFloat($(".resultado-1 .chegada-" + letra).val());
            let u = parseFloat($(".resultado-1 .servico-" + letra).val());
            let temp = (1.0 - (a / u)) * Math.pow((a / u), i);
            html += "</td><td>";
            html += temp.toFixed(2);
        }
        html += "</td></tr>";
        
        $(".resultado-3").append(html);
    }
    
}