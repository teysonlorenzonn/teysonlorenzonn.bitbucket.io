const somenteNumeros = (ev) => {
    const value = ev.target.value.replace(/\D/g, '');
    ev.target.value = value;
}

const somenteMaiusculasSemEspaco = (ev) => {
    const value = ev.target.value.replace(/ /g, '');
    ev.target.value = value.toUpperCase().trim();
}

