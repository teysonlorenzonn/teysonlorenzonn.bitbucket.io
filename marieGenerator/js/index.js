

let nodeClone = null;
let countNode = 0;
let countFor = 0;
const ids = [];
let keys = {};
let vars = [];
let marie = {
    body: [],
    footer: []
}

const monta = [];

function allowDrop(ev) {
    ev.preventDefault();
}
  
function drag(ev) {
    nodeClone = document.getElementById(ev.target.id);
    if (nodeClone.parentNode.id === "solutions") {
        const id = `${nodeClone.id}-${countNode}`;
        let value = null;
        if(nodeClone.id === 'if') {
            value = nodeClone.children[1].children[1].value;
        }
        nodeClone = nodeClone.cloneNode(true);
        nodeClone.id = id;
        nodeClone.children[0].style = "display: block !important;";
        if(value) {
            nodeClone.children[1].children[1].value = value;
        }
        countNode++;
    }
}

function drop(ev) {
    ev.preventDefault();
    const id = ev.target.id;
    if(/for/.test(nodeClone.id) && /for-content/.test(id)) {
        return
    }

    if (/mount|for-content/.test(id)) {
        const idPuro = nodeClone.id.split('-')[0];
       
        const gerado = gerarDicionario(idPuro, nodeClone);
        
        if (gerado) {
            ev.target.appendChild(nodeClone);
            remontarDicionario(ev, true);
        }
    }

}

function remontarDicionario(ev, drag = false) {
    marie = {
        body: [],
        footer: [],
    }
    countFor = 0;
    vars = [];
    
  

    const mountFilhos = document.getElementById('mount').children;
    for(let i = 0; i < mountFilhos.length; i++) {
        if(i > 0) {
            const idPuro = mountFilhos[i].id.split('-')[0];
            if(/for/.test(mountFilhos[i].id)) {
                if(countFor === 0) {
                    marie.footer.push(`ONE, DEC 1`);
                }
                marie.body.push(gerarEspacoFor(false, `LOAD FORUNTIL${countFor}`));
                marie.body.push(gerarEspacoFor(false, `SUBT FORIN${countFor}`));
                marie.body.push(gerarEspacoFor(false, `STORE FORUNTIL${countFor}`));
                const content = mountFilhos[i].children[2];
                let montou = false;
                for(let j = 0; j < content.children.length; j++) {
                    const id =  content.children[j].id.split('-')[0];
                    if(id !== 'atribuirvariavel') {
                        if(!montou) {
                            gerarDicionario(id, content.children[j], true, true);
                            montou = true;
                        }else {
                            gerarDicionario(id, content.children[j], true, false);
                        }
                        
                    }else {
                        gerarDicionario(id, content.children[j], true, false);
                    }
                }
                marie.body.push(gerarEspacoFor(true, '\n'));
                marie.body.push(gerarEspacoFor(true, `LOAD FORUNTIL${countFor}`));
                marie.body.push(gerarEspacoFor(true, `SUBT ONE`));
                marie.body.push(gerarEspacoFor(true, `STORE FORUNTIL${countFor}`));
                marie.body.push(gerarEspacoFor(true, `SKIPCOND 000`));
                marie.body.push(gerarEspacoFor(true, `JUMP LOOP${countFor > 0 ? countFor : ''}`));
                marie.body.push(gerarEspacoFor(true, '\n'));
            }
            gerarDicionario(idPuro, mountFilhos[i], false, 0);   
        }
    }
    generateMarie(ev);
}

function gerarEspacoFor(isFor, value, monta) {
    if(!isFor) {
        return value;
    }
    if(monta) {
        const loopVar = `LOOP${countFor > 0 ? countFor : ''}`
        if(!vars.includes(loopVar)) {
            vars.push(loopVar);
        }
        return `${loopVar},    ${value}`;
    }
    return `              ${value}`;
}

function gerarDicionario(idPuro, nodeClone, isFor = false, pos) {
    let include = false;
    let variavel = null;
    let de = null;
    let ate = null;
    let value = null;

    switch (idPuro) {
        case 'for':
            de = nodeClone.children[1].children[1].value;
            ate = nodeClone.children[1].children[3].value;
            if(de && ate) {
                include = true;
                if(!Number(de)) {
                    marie.footer.forEach(e => {
                        const splitE = e.split(',');
                        if(splitE[0] === de) {
                            de = splitE[1].trim().replace('DEC ', '');
                        }
                    });
                }
                if(!Number(ate)) {
                    marie.footer.forEach(e => {
                        const splitE = e.split(',');
                        if(splitE[0] === ate) {
                            ate = splitE[1].trim().replace('DEC ', '');
                        }
                    });
                }

                if(parseInt(de) > parseInt(ate)) {
                    marie.footer.push(`FORIN${countFor}, DEC ${ate}`);
                    marie.footer.push(`FORUNTIL${countFor}, DEC ${de}`);
                }else {
                    marie.footer.push(`FORIN${countFor}, DEC ${de}`);
                    marie.footer.push(`FORUNTIL${countFor}, DEC ${ate}`);
                }
                countFor++;
            }
            break;
        case 'if':
            value = nodeClone.children[1].children[1].value
            if(value) {
                variavel = nodeClone.children[2].children[0].children[0].children[1].value.toUpperCase()
                if(variavel) {
                    if(!vars.includes(variavel.toUpperCase())) {
                        alert('Variavel não está declarada')
                    } else {
                        marie.body.push(gerarEspacoFor(isFor, `SKIPCOND ${value}`, pos));
                        marie.body.push(gerarEspacoFor(isFor, `JUMP ${variavel}`, pos));
                        include = true;
                    }
                }else {
                    alert('Campo deve estar preechidos')
                }
            }else {
                alert('Uma opção deve ser escolhida')
            }
            break;
        case 'atribuirvariavel':
            variavel = nodeClone.children[1].children[1].value.toUpperCase();
            value = nodeClone.children[1].children[3].value;
            if(variavel && value) {
                
                const resp = `${variavel}, DEC ${value}`;
                if(!vars.includes(variavel)) {
                    vars.push(variavel);
                    marie.footer.push(resp);
                    include = true;
                }else {
                    marie.footer.forEach((e, idx) => {
                        const val = e.split(',')[0];
                        if(val === variavel) {
                            marie.footer[idx] = resp;
                            include = true;
                        }
                    });
                }
            }else {
                alert('Ambos os campos devem estar preechidos')
            }
            break;
        case 'guardavariavel':
            variavel = nodeClone.children[1].children[1].value.toUpperCase()
            if(variavel) {
                if(!vars.includes(variavel)) {
                    vars.push(variavel);
                    marie.footer.push(`${variavel}, DEC 0`);
                }
                marie.body.push(gerarEspacoFor(isFor, `STORE ${variavel}`, pos));
                include = true;
            }else {
                alert('Campo deve estar preechidos')
            }
            break;
        case 'lervariavel':
            marie.body.push(gerarEspacoFor(isFor, 'INPUT', pos));
            include = true;
            break;
        case 'sum':
            variavel = nodeClone.children[1].children[1].value;
            if(!vars.includes(variavel.toUpperCase())) {
                alert('variavel não está declarada')
            } else {
                marie.body.push(gerarEspacoFor(isFor,`ADD ${variavel.toUpperCase()}`, pos));
                include = true;
            }
            break;
        case 'sub': 
            variavel = nodeClone.children[1].children[1].value;
            if(!vars.includes(variavel.toUpperCase())) {
                alert('variavel não está declarada')
            } else {
                marie.body.push(gerarEspacoFor(isFor,`SUBT ${variavel.toUpperCase()}`, pos));
                include = true;
            }
            break;
        case 'carregar':
            variavel = nodeClone.children[1].children[1].value.toUpperCase()
            if(variavel) {
                if(vars.includes(variavel)) {
                    marie.body.push(gerarEspacoFor(isFor,`LOAD ${variavel.toUpperCase()}`, pos));
                    include = true;
                   
                }else {
                    alert('A variavel não está declarada');
                }
            }else {
                alert('Campo deve estar preechidos');
            }
            break;
        case 'imprimir':
            marie.body.push(gerarEspacoFor(isFor,'OUTPUT', pos));
            include = true;
        default:
            break;
    }
    return include
}

function onDelete(ev) {
    ev.preventDefault();
    const div = ev.target.parentNode.parentNode;
    ids.splice(ids.indexOf(div.id),1);
    div.parentNode.removeChild(div);
    remontarDicionario(ev);

}

function generateMarie(ev) {
    ev.preventDefault();

    const nodetx = document.getElementById('tx-result');
    const {body, footer} = marie;

    if(body.length || footer.length) {
        document.getElementById('final').style.display = 'block';
    }else {
        document.getElementById('final').style.display = 'none';
    }
   
    removeAll(nodetx);
    nodetx.value = 'CLEAR\n';
    body.forEach((e,idx) => {
        nodetx.value += e + '\n';
        if(idx === body.length - 1) {
            nodetx.value += 'HALT\n\n';
        }
        espandir(nodetx);
    });

    footer.forEach(e => {
        nodetx.value += e + '\n';
        espandir(nodetx);
    });
  
}

function removeAll(node){
    node.value = "";
}

document.addEventListener('DOMContentLoaded', function() {
    const elems = document.querySelectorAll('.tooltipped');
    M.Tooltip.init(elems, {
        html: 'teste',
        position: 'bottom'
    });
});

function vaipara() {
    window.open("https://marie.js.org/", "_blank");
}

function espandir(txtAreas) {
    while($(txtAreas).outerHeight() < txtAreas.scrollHeight + parseFloat($(txtAreas).css("borderTopWidth")) + parseFloat($(txtAreas).css("borderBottomWidth"))) {
        $(txtAreas).height($(txtAreas).height()+1);
    };
}

function copyText() {
    const nodetx = document.getElementById('tx-result');

    nodetx.select();
    nodetx.setSelectionRange(0, 99999);

    navigator.clipboard.writeText(nodetx.value);

    alert('Código copiado com sucesso!')
}