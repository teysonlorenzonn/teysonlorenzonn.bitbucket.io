$("#simulate").click(() => {
    let table = $(".resultado-1 tbody");
    table.html("");
    let tempoLimite = $("input#tempo").val();

    let cliente = 0;
    let tempoDesdeAUltimaChegada = 0;
    let tempoDeChagadaNoRelogio = 0;
    let tempoDeServico = 0;
    let tempoDeInicioDoServicoNoRelogio = 0;
    let tempoDoClienteNaFila = 0;
    let tempoFinalDoServicoNoRelogio = 0;
    let tempoDoClienteNoSistema = 0;
    let tempoLivreDoOperador = 0;
    
    let tempoTotalDeEsperaNaFila = 0;
    let tempoTotalDoOperadorLivre = 0;
    let tempoTotalDoServico = 0;
    let tempoTotalDoClienteNoSistema = 0;
    
    while (tempoDeChagadaNoRelogio < tempoLimite){
        tempoDesdeAUltimaChegada = parseInt(getTEC());
        tempoDeServico = parseInt(getTS());
        tempoTotalDoServico += tempoDeServico;
        tempoDeChagadaNoRelogio = tempoDeChagadaNoRelogio + tempoDesdeAUltimaChegada;
        
        let tempoChegada = 0;
        
        tempoClienteNaFila = 0;
        if (cliente > 1){
            let x = parseInt(table.find("tr:last td:eq(2)").html());
            if (x > tempoChegada){
                tempoClienteNaFila = x - tempoChegada;
            }
        }
        
        if (cliente == 0){
            tempoDeInicioDoServicoNoRelogio = tempoDesdeAUltimaChegada;
            tempoFinalDoServicoNoRelogio = tempoDeInicioDoServicoNoRelogio + tempoDeServico;
            tempoLivreDoOperador = tempoDeChagadaNoRelogio;
        }
        
        if (cliente > 0){
            let x = parseInt(table.find("tr:last td:eq(6)").html());
            tempoDoClienteNaFila = 0;
            if ( x > tempoDeChagadaNoRelogio){
                tempoDoClienteNaFila = x - tempoDeChagadaNoRelogio;
            }
            
            tempoTotalDeEsperaNaFila += tempoDoClienteNaFila;
            
            tempoDeInicioDoServicoNoRelogio = tempoDoClienteNaFila + tempoDeChagadaNoRelogio;
            
            tempoFinalDoServicoNoRelogio = tempoDeServico + tempoDeInicioDoServicoNoRelogio;
            
            tempoLivreDoOperador = 0;
            if (x < tempoDeChagadaNoRelogio){
                tempoLivreDoOperador = tempoDeChagadaNoRelogio - x;
            }
            tempoTotalDoOperadorLivre += tempoLivreDoOperador;
        }
        
        tempoDoClienteNoSistema = tempoFinalDoServicoNoRelogio - tempoDeChagadaNoRelogio;
        tempoTotalDoClienteNoSistema += tempoDoClienteNoSistema;
        
        if (cliente > 0){
            tempoFinalDoServicoNoRelogio = tempoDeInicioDoServicoNoRelogio + tempoDeServico;
        }
        tempoInicioServico = (tempoClienteNaFila + tempoChegada);
        let html = "<tr>";
        html += "<td>" + (++cliente) + "</td>";
        html += "<td>" + tempoDesdeAUltimaChegada + "</td>";
        html += "<td>" + tempoDeChagadaNoRelogio + "</td>";
        html += "<td>" + tempoDeServico + "</td>";
        html += "<td>" + tempoDeInicioDoServicoNoRelogio + "</td>";
        let dataClienteEmFila = "";
        if (tempoDoClienteNaFila > 0){
            dataClienteEmFila = "data-cliente-na-fila=\"true\"";
        }
        html += "<td " + dataClienteEmFila + ">" + tempoDoClienteNaFila + "</td>";
        html += "<td>" + tempoFinalDoServicoNoRelogio + "</td>";
        html += "<td>" + tempoDoClienteNoSistema + "</td>";
        html += "<td>" + tempoLivreDoOperador + "</td>";
        html += "</tr>";
        table.html(table.html() + html);
    }
    
    $(".resultado-1 tfoot tr td:eq(3)").html(tempoTotalDoServico);
    $(".resultado-1 tfoot tr td:eq(5)").html(tempoTotalDeEsperaNaFila);
    $(".resultado-1 tfoot tr td:eq(7)").html(tempoTotalDoClienteNoSistema);
    $(".resultado-1 tfoot tr td:eq(8)").html(tempoTotalDoOperadorLivre);
    
    let quantidadeClienteEmFila = $("td[data-cliente-na-fila=true]").length;
    $("#total-clientes").html(cliente);
    $("#media-espera").html((tempoTotalDeEsperaNaFila / cliente).toFixed(2));
    $("#probabilidade-cliente-fila").html((quantidadeClienteEmFila / cliente).toFixed(2));
    $("#probabilidade-operador-livre").html((tempoTotalDoOperadorLivre / tempoFinalDoServicoNoRelogio).toFixed(2));
    $("#media-servico").html((tempoTotalDoServico / cliente).toFixed(2));
    $("#tempo-medio-gasto").html((tempoTotalDoClienteNoSistema / cliente).toFixed(2));
    $("#table1").removeClass("display-none");
    $("#table2").removeClass("display-none");
});

$("#clear").click(() => {
    $("input[type=text]").val("");
    $("ul#ts").html("");
    $("ul#tec").html("");
    $(".resultado-1 tbody").html("");

    $(".resultado-1 tfoot tr td:eq(3)").html("");
    $(".resultado-1 tfoot tr td:eq(5)").html("");
    $(".resultado-1 tfoot tr td:eq(7)").html("");
    $(".resultado-1 tfoot tr td:eq(8)").html("");

    $("#total-clientes").html("");
    $("#media-espera").html("");
    $("#probabilidade-cliente-fila").html("");
    $("#probabilidade-operador-livre").html("");
    $("#media-servico").html("");
    $("#tempo-medio-gasto").html("");
    $("#table1").addClass("display-none")
    $("#table2").addClass("display-none")
});
$("#addTEC").click(() => {
    let val = $("input#tec").val();
    let ul = $("ul#tec");
    let html = "<li class=\"list-group-item\">" + val + "</ul>";
    html = ul.html() + html;
    ul.html(html);
});

$("#addTS").click(() => {
    let val = $("input#ts").val();
    let ul = $("ul#ts");
    let html = "<li class=\"list-group-item\">" + val + "</ul>";
    html = ul.html() + html;
    ul.html(html);
});

const getTEC = () => {
    let ulTecLi = $("ul#tec li");
    let MAX = ulTecLi.length;
    let x = Math.floor((Math.random() * MAX)) - 1;
    return $("ul#tec li:eq(" + x + ")").html();
}

const getTS = () => {
    let ulTecLi = $("ul#ts li");
    let MAX = ulTecLi.length;
    let x = Math.floor((Math.random() * MAX)) - 1;
    return $("ul#ts li:eq(" + x + ")").html();
}


