const button = $('#generate');
const remove = $('#remove');
const inputOne = $('#inputOne');
const inputTwo = $('#inputTwo');
const inputTree = $('#inputTree');
const inputFour = $('#inputFour');
const labelOne = $('#labelOne');
const labelTwo = $('#labelTwo');
const labelTree = $('#labelTree');
const typeConvert = $('#typeConvert');
const results = $('#results');
const print = $('#print');

let type = 'rgb';

$(document).ready(function () {
    $('select').formSelect();
    insertValuesLabel(type);

});

const insertValuesLabel = (value) => {
    switch (value) {
        case 'rgb':
            labelOne.html('R:');
            labelTwo.html('G:');
            labelTree.html('B:');
            $('#divK').addClass('div-k');
            break;
        case 'hsv':
            labelOne.html('H:');
            labelTwo.html('S:');
            labelTree.html('V:');
            $('#divK').addClass('div-k');
            break;
        case 'cmyk':
            labelOne.html('C:');
            labelTwo.html('M:');
            labelTree.html('Y:');
            inputFour.removeAttr("style");
            $('#divK').removeClass('div-k');
            break;
        default:
            break;
    }

}

inputOne.change(e => inputOne.val(maskNumber(e.target.value.toString(), 'one')));
inputTwo.change(e => inputTwo.val(maskNumber(e.target.value.toString(), 'two')));
inputTree.change(e => inputTree.val(maskNumber(e.target.value.toString(), 'tree')));
inputFour.change(e => inputFour.val(maskNumber(e.target.value.toString(), 'four')));

const maskNumber = (value, input) => {
    value.replace(/\D/g, '');
    if (type === 'rgb') {
        if (parseInt(value) >= 255) value = 255;
        if (parseInt(value) <= 0) value = 0;
        return value;
    }
    else if (type === 'hsv') {
        switch (input) {
            case 'one':
                if (parseInt(value) >= 360) value = 360;
                break;
            case 'two':
                if (parseInt(value) >= 100) value = 100;
                break;
            case 'tree':
                if (parseInt(value) >= 100) value = 100;
                break;
        }
        if (parseInt(value) <= 0) value = 0;
        return value;
    }
    else if (type === 'cmyk') {
        if (parseInt(value) >= 100) value = 100;
        return value;
    }
};

const maskNumberHSV = () => {

}

const normalizeRGB = () => {
    let r = parseFloat(inputOne.val());
    let g = parseFloat(inputTwo.val());
    let b = parseFloat(inputTree.val());
    let nr = r / (r + g + b);
    let ng = g / (r + g + b);
    let nb = b / (r + g + b);

    return {
        nr: nr.toFixed(2),
        ng: ng.toFixed(2),
        nb: nb.toFixed(2)
    };
};

const convetCMYKxRGB = () => {
    let c = parseFloat(inputOne.val());
    let m = parseFloat(inputTwo.val());
    let y = parseFloat(inputTree.val());
    let k = parseFloat(inputFour.val());

    c /= 100;
    m /= 100;
    y /= 100;
    k /= 100;

    let R = 1 - Math.min(1, c * (1 - k) + k);
    let G = 1 - Math.min(1, m * (1 - k) + k);
    let B = 1 - Math.min(1, y * (1 - k) + k);

    R = Math.round(R * 255);
    G = Math.round(G * 255);
    B = Math.round(B * 255);
    return {
        r: R,
        g: G,
        b: B
    };
};

const convetRGBxCMYK = () => {
    let r = parseFloat(inputOne.val()) / 255;
    let g = parseFloat(inputTwo.val()) / 255;
    let b = parseFloat(inputTree.val()) / 255;

    let k = 1 - Math.max(r + g + b);
    let c = (1 - r - k) / (1 - k);
    let m = (1 - g - k) / (1 - k);
    let y = (1 - b - k) / (1 - k);

    return {
        c: c,
        m: m,
        y: y,
        k: k
    }
};

const covertHSVxRGB = () => {
    let h = parseFloat(inputOne.val());
    let s = parseFloat(inputTwo.val());
    let v = parseFloat(inputTree.val());
    var r, g, b;
    var i;
    var f, p, q, t;

    h = Math.max(0, Math.min(360, h));
    s = Math.max(0, Math.min(100, s));
    v = Math.max(0, Math.min(100, v));

    s /= 100;
    v /= 100;

    if (s == 0) {
        r = g = b = v;
        return [
            Math.round(r * 255),
            Math.round(g * 255),
            Math.round(b * 255)
        ];
    }

    h /= 60;
    i = Math.floor(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - s * f);
    t = v * (1 - s * (1 - f));

    switch (i) {
        case 0:
            r = v;
            g = t;
            b = p;
            break;

        case 1:
            r = q;
            g = v;
            b = p;
            break;

        case 2:
            r = p;
            g = v;
            b = t;
            break;

        case 3:
            r = p;
            g = q;
            b = v;
            break;

        case 4:
            r = t;
            g = p;
            b = v;
            break;

        default:
            r = v;
            g = p;
            b = q;
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
    };
};

const covertRGBxHSV = () => {
    let R = parseFloat(inputOne.val()) / 255;
    let G = parseFloat(inputTwo.val()) / 255;
    let B = parseFloat(inputTree.val()) / 255;
    let cMax = parseFloat(Math.max(R, G, B));
    let cMin = parseFloat(Math.min(R, G, B));
    let delta = cMax - cMin;
    let H = cMax;
    let S = cMax;
    let V = (cMax * 100).toFixed(1);

    switch (cMax) {
        case cMin:
            H = '0';
            break;
        case R:
            H = Math.round(60 * (((G - B) / delta) + (G < B ? 6 : 0))).toString();
            break;
        case G:
            H = Math.round(60 * (((B - R) / delta) + 2)).toString();
            break;
        case B:
            H = Math.round(60 * ((R - G) / delta + 4)).toString();
            break;
        default:
            break;
    }

    if (cMax === cMin) {
        S = '0';
    } else {
        S = ((delta / cMax) * 100).toFixed(1);
    }

    return {
        h: H,
        s: S,
        v: V
    }
};

const convetRGBxGrey = () => ((parseFloat(inputOne.val()) + parseFloat(inputTwo.val()) + parseFloat(inputTree.val())) / 3).toFixed(2).toString();

const clearInputs = () => {
    inputOne.val('');
    inputTwo.val('');
    inputTree.val('');
    inputFour.val('');
}

typeConvert.change(e => {
    type = e.target.value;
    insertValuesLabel(e.target.value);
    clearInputs();
})

remove.click(() => {
    clearInputs();
})

const componentToHex = function (rgb) {
    let hex = Number(rgb).toString(16);
    if (hex.length < 2) {
        hex = "0" + hex;
    }
    return hex;
};

const rgbToHex = ({ r, g, b }) => {
    return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
}

button.click(async () => {
    let text = '';
    let color = '';
    switch (typeConvert.val()) {
        case 'rgb':
            let rgb = await normalizeRGB();
            let hsv = await covertRGBxHSV();
            color = rgbToHex({ r: inputOne.val(), g: inputTwo.val(), b: inputTree.val() });
            results.html(text)
            text = `NR:${rgb.nr} | NG:${rgb.ng} | NB:${rgb.nb}<br/>H:${hsv.h} | S:${hsv.s} | V: ${hsv.v}<br/>Grey:${convetRGBxGrey()}`;
            break;
        case 'hsv':
            let rgbh = await covertHSVxRGB();
            results.html(text)
            text = `R:${rgbh.r} | G:${rgbh.g} | B:${rgbh.b}`;
            color = rgbToHex(rgbh);
            break;
        case 'cmyk':
            let cmyk = await convetCMYKxRGB();
            results.html(text)
            text = `R:${cmyk.r} | G:${cmyk.g} | B:${cmyk.b}`;
            color = rgbToHex(cmyk);
            break;
    }
    print.removeAttr("style");
    print.attr("style", `background-color:${color};`);
    results.html(text);

});