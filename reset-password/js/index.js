let validPass = false;
let validConf = false;


$('#eye-two').click((event) => {
    event.preventDefault();
    $('#eye-two').hide();
    $('#eye-two-disabled').show();
    $('#confirmPassword').get(0).type = 'text'
});


$('#eye-two-disabled').click((event) => {
    event.preventDefault();
    $('#eye-two').show();
    $('#eye-two-disabled').hide();
    $('#confirmPassword').get(0).type = 'password'
});


$('#eye-one').click((event) => {
    event.preventDefault();
    $('#eye-one').hide();
    $('#eye-one-disabled').show();
    $('#password').get(0).type = 'text'
});


$('#eye-one-disabled').click((event) => {
    event.preventDefault();
    $('#eye-one').show();
    $('#eye-one-disabled').hide();
    $('#password').get(0).type = 'password'
});

$('#confirm').click(async (event) => {
    event.preventDefault();
    const password = $('#password').val();
    const confirmPassword = $('#confirmPassword').val();

    if(validPass && validConf) {
        const search = window.location.search.slice(1)
        const queryArr = search.split('&')
        const query = {};
        let resp;
        queryArr.forEach((e) => {
            const chaveValor = e.split('=');
            const chave = chaveValor[0];
            const valor = chaveValor[1];
            query[chave] = valor;
        });
        await fetch('https://london-barber-apis.herokuapp.com/user/password', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                hash: query.hash ?? null,
                password,
                confirmPassword
            })
        })
        .then(response => response.json())
        .then(response => (resp = response))
        .catch(error => (resp = error));
        $('#message').html(resp?.message ?? 'Servidor em manutenção')  
    }
});

$('#password').keyup(event => {
    const validation = hasPass(event.target.value, $('#confirmPassword').val())
    $('#error-one').html(validation.msg)
    $('#error-two').html(validation.validTwo && validation.validOne
        ? ''
        : $('#error-two').html());
    validPass = validation.validOne;
    validConf = validation.validTwo;
})

$('#confirmPassword').keyup(event => {
    const validation = hasPass(event.target.value, $('#password').val())
    $('#error-two').html(validation.msg)
    $('#error-one').html(validation.validTwo && validation.validOne
        ? ''
        : $('#error-one').html());
    validConf = validation.validOne;
    validPass = validation.validTwo;
})

const hasPass = (passOne, passTwo) => {
    if (passOne.length === 0) {
      return {validOne: false, msg: '', validTwo: false};
    }
    if (passOne.length < 6) {
      return {
        validOne: false,
        msg: 'A senha deve conter 6 ou mais caracteres',
        validTwo: false,
      };
    }
    if (passOne === passTwo) {
      return {
        validOne: true,
        msg: '',
        validTwo: true,
      };
    }
  
    return {
      validOne: false,
      msg: 'As senhas não conferem',
      validTwo: true,
    };
  };